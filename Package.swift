// swift-tools-version:5.5
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
	name: "Chirp",
	platforms: [
		.macOS(.v10_12)
	],
	products: [
		.library(
			name: "Chirp",
			targets: ["Chirp"]),
	],
	dependencies: [
		.package(url: "https://github.com/1024jp/GzipSwift", from: "5.0.0"),
		.package(url: "https://github.com/dankogai/swift-bignum.git", .branch("main"))
	],
	targets: [
		.target(
			name: "Chirp",
			dependencies: [
				.product(name: "Gzip", package: "GzipSwift"),
				.product(name: "BigNum", package: "swift-bignum")
			],
			resources: [
				.copy("empty_clip.xml"),
			]
		),
		.testTarget(
			name: "ChirpTests",
			dependencies: ["Chirp"]
		),
	]
)
