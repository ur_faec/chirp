//
//  ALC.swift
//  
//
//  Created by fae on 11/23/21.
//

import Foundation

import Gzip

typealias Cents = Double

struct ALCPitch {
	// The midi index of the target pitch
	let index: Int8
	
	// The offset from the standard tuning of the target pitch, in cents
	let offset: Cents
}

extension Note {
	var alcPitch: ALCPitch {
		let midiPitch = self.pitch + 60.0
		let rounded = round(midiPitch)
		let clamped = max(min(rounded, 127), 0)
		let index = Int8(clamped)
		let offset = 100.0 * (midiPitch - clamped)
		print("alcPitch: \(self.pitch) -> \(index)")
		return ALCPitch(index: index, offset: Cents(offset))
	}
}

extension Rational {
	func clamped() -> Rational {
		let rounded = (self * 1000000).rounded() / 1000000
		return rounded
	}
	func clampedLow() -> Rational {
		let rounded = (self * 1000000 - 1).rounded() / 1000000
		return rounded
	}
}

struct ALCData {
	var name: String
	var duration: Rational
	var keys: [Int8: [Note]] = [:]
	var nextKeytrackId: Int = 0
	var nextNoteId: Int = 1
	
	
	mutating func addNote(_ note: Note) {
		let index = note.alcPitch.index
		var keyTrack = keys[index] ?? []
		keyTrack.append(note)
		keys[index] = keyTrack
	}
}

func midiNoteEventForNote(_ note: Note, noteId: Int) -> XMLElement {
	let midiEvent = XMLElement(name: "MidiNoteEvent")
	let start = note.time.start.clamped()
	let end = note.time.end.clampedLow()
	let duration = end - start
	midiEvent.setAttributesWith([
		"Time":"\(start.toFloatingPointString())",
		"Duration": "\(duration.toFloatingPointString())",
		"Velocity": "\(note.midiVelocity)",
		"VelocityDeviation": "0",
		"OffVelocity": "64",
		"Probability": "1",
		"IsEnabled": "true",
		"NoteId": "\(noteId)",
	])
	return midiEvent
}

func keyTrackForId(_ keyTrackId: Int, midiKey: Int8, notes: [Note]) -> XMLElement {
	let keyTrack = XMLElement(name: "KeyTrack")
	keyTrack.setAttributesWith(["Id": "\(keyTrackId)"])
	
	let notesElement = XMLElement(name: "Notes")
	let midiEvents = (0..<notes.count).map { i in
		midiNoteEventForNote(notes[i], noteId: i)
	}
	notesElement.setChildren(midiEvents)
	
	let midiKeyElement = XMLElement(name: "MidiKey")
	midiKeyElement.setAttributesWith(["Value": "\(midiKey)"])
	
	keyTrack.setChildren([notesElement, midiKeyElement])
	
	return keyTrack
}

enum ALCError: Error {
	case noEmptyClip
}

public func checkEmptyClip() -> URL? {
	//return Bundle(identifier: "Chirp")?.url(forResource: "empty_clip", withExtension: "xml")
	if runningInPlayground() {
		let home = FileManager.default.homeDirectoryForCurrentUser
		return URL(fileURLWithPath: "code/swift/Chirp/Sources/Chirp/empty_clip.xml", relativeTo: home)
	}
	return Bundle.module.url(forResource: "empty_clip", withExtension: "xml")
}

public func checkBundles() -> [Bundle] {
	return Bundle.allFrameworks.filter {
		$0.bundlePath.range(of: "Chirp") != nil
	}
}

extension ALCData {
	public func xmlDocument() -> Result<XMLDocument, ALCError> {
		
		guard let templateURL = checkEmptyClip()
						//Bundle.module.url(forResource: "empty_clip", withExtension: "xml")
		else {
			print("couldn't find empty_clip.xml")
			return .failure(.noEmptyClip)
		}
		print("reading empty clip from \(templateURL)")
		
		let doc = try! XMLDocument(contentsOf: templateURL)
		let root = doc.rootElement()
		let midiClip = (
			root!.elements(forName: "LiveSet").first!
				.elements(forName: "Tracks").first!
				.elements(forName: "MidiTrack").first!
				.elements(forName: "DeviceChain").first!
				.elements(forName: "MainSequencer").first!
				.elements(forName: "ClipSlotList").first!
				.elements(forName: "ClipSlot").first!
				.elements(forName: "ClipSlot").first!
				.elements(forName: "Value").first!
				.elements(forName: "MidiClip").first!
		)
		let trackName = midiClip.elements(forName: "Name").first!
		trackName.setAttributesWith(["Value": name])
		
		let notes = midiClip.elements(forName: "Notes").first!
		let keyTracks = notes.elements(forName: "KeyTracks").first!
		var keyTrackId = 0
		var children: [XMLElement] = []
		for (index, notes) in keys {
			let keyTrack = keyTrackForId(keyTrackId, midiKey: index, notes: notes)
			children.append(keyTrack)
			keyTrackId += 1
		}
		keyTracks.setChildren(children)
		
		let noteIdGenerator = notes.elements(forName: "NoteIdGenerator").first!
		
		let nextId = XMLElement(name: "NextId")
		nextId.setAttributesWith(["Value": "\(children.count + 1)"])
		noteIdGenerator.setChildren([nextId])
		return .success(doc)
	}


	func saveToTempFile() -> Result<URL, Error> {
		let temporaryDirectoryURL = URL(fileURLWithPath: NSTemporaryDirectory(),
																		isDirectory: true)
		let outputURL = temporaryDirectoryURL.appendingPathComponent("generated.alc")
		let xmlURL = temporaryDirectoryURL.appendingPathComponent("generated.xml")
		let doc = try! xmlDocument().get()
		let str = doc.xmlString(options: [.nodePrettyPrint, .nodeCompactEmptyElement])
		let data = str.data(using: .utf8)!
		try! data.write(to: xmlURL)
		let gzipped = try! data.gzipped()
		try! gzipped.write(to: outputURL)
		return .success(outputURL)
	}
}

public func saveToTempFile(clip: Clip) -> Result<URL, Error> {
	var alcData = ALCData(name: "hi fae", duration: clip.duration)
	for note in clip.notes {
		alcData.addNote(note)
	}
	return alcData.saveToTempFile()
}

