import Foundation

public struct Clip: Hashable, Equatable {
	public let duration: Rational
	public let notes: [Note]
	
	public init(duration: Rational, notes: [Note]) {
		self.duration = duration
		self.notes = notes
	}
	
	public func mapTime(_ f: (Rational) -> Rational) -> Clip {
		return Clip(
			duration: f(duration),
			notes: notes.map {
				$0.withTime(TimeSpan(
					start: f($0.time.start),
					end: f($0.time.end)))
			})
	}
}

public extension Note {
	func asClip() -> Clip {
		return Clip(duration: time.end, notes: [self])
	}
}

public func Repeat(_ clip: Clip, count: Int) -> Clip {
	var notes: [Note] = []
	for i in 0..<count {
		let curTime = Rational(i) * clip.duration
		let curNotes = clip.notes.map { $0.offsetBy(curTime) }
		notes.append(contentsOf: curNotes)
	}
	return Clip(duration: Rational(count) * clip.duration, notes: notes)
}

public func Fit(_ clip: Clip, into duration: Rational) -> Clip {
	let scale = duration / clip.duration
	let notes = clip.notes.map { $0.scaledBy(scale) }
	return Clip(duration: duration, notes: notes)
}
