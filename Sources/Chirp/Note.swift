//
//  Note.swift
//  
//
//  Created by fae on 11/23/21.
//

import Foundation

public struct Note: Hashable, Equatable {
	public let time: TimeSpan
	
	public let pitch: Double
	public let velocity: Double
	
	public init(time: TimeSpan, pitch: Double = 0.0, velocity: Double = 1.0) {
		self.time = time
		self.pitch = pitch
		self.velocity = velocity
	}
	
	public init(duration: Rational = 1) {
		self.init(time: TimeSpan(start: 0.0, end: duration))
	}
	
	public func withTime(_ time: TimeSpan) -> Note {
		return Note(time: time, pitch: self.pitch, velocity: self.velocity)
	}
}

extension Note {

	public func offsetBy(_ delta: Rational) -> Note {
		return Note(time: time.offsetBy(delta), pitch: pitch, velocity: velocity)
	}
	
	public func scaledBy(_ scale: Rational) -> Note {
		return Note(time: time.scaledBy(scale), pitch: pitch, velocity: velocity)
	}

	public var midiVelocity: Int8 {
		return Int8(max(min(floor(velocity * 127.9), 127), 0))
	}
}
