//
//  Time.swift
//  
//
//  Created by fae on 11/26/21.
//

import Foundation

import BigNum

public typealias Rational = BigRat

public struct TimeSpan: Hashable, Equatable {
	public let start: Rational
	public let end: Rational
	
	public init(start: Rational, end: Rational) {
		self.start = start
		self.end = end
	}
	
	public var duration: Rational {
		return end - start
	}
	
	public func offsetBy(_ delta: Rational) -> TimeSpan {
		return TimeSpan(start: start + delta, end: end + delta)
	}
	
	public func scaledBy(_ scale: Rational) -> TimeSpan {
		return TimeSpan(start: scale * start, end: scale * end)
	}
}
