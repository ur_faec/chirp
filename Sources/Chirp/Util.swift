//
//  Util.swift
//  
//
//  Created by fae on 11/23/21.
//

import Foundation

func runningInPlayground() -> Bool {
	return Bundle.allBundles.contains { ($0.bundleIdentifier ?? "").hasPrefix("com.apple.dt.") }
}
